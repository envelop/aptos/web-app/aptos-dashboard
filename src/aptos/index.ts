import { getDAFromChain, 
    localStorageGet, 
    localStorageSet, 
    localStorageRemove, 
    NetworkByID, 
    getAptosNativeBalance,
    isAptosAddress,
    mintDA
     } from './aptosUtils';
/*
import {
	getUserNFTsFromAPI,
	getUserWNFTsFromAPI,
     } from './oracle';

import {     fetchTokenJSON,
     } from './fetchtokenwrapper';

import {getWNFTsOfContractFromChain721,
    getWNFTsOfContractFromChain,
    getNFTsOfContractFromChain
     } from './chain';
*/
export * from './_types';
export * from './_utils';
export * from './chain';
export * from './oracle';
export * from './fetchtokenwrapper';

export { getDAFromChain, 
    localStorageGet, 
    localStorageSet, 
    localStorageRemove, 
    NetworkByID, 
    getAptosNativeBalance,
    isAptosAddress,
    mintDA,/*
    fetchTokenJSON,
	getUserNFTsFromAPI,
	getUserWNFTsFromAPI,
	getWNFTsOfContractFromChain721,
    getWNFTsOfContractFromChain,
    getNFTsOfContractFromChain*/
};

let _sessionCache: Array<{ key: string, data: any }> = [];
export const _setCacheItem = (key: string, data: any) => {
	_sessionCache = [
		..._sessionCache.filter((item: { key: string, data: any }) => { return item.key !== key }),
		{ key, data }
	]
}
export const _getCacheItem = (key: string): any | undefined => {
	const foundItem = _sessionCache.find((item: { key: string, data: any }) => { return item.key === key });
	return foundItem?.data || undefined;
}