import Web3 from 'web3';
import { PromiEvent } from 'web3-core';
import { Contract } from 'web3-eth-contract';
import {
	fetchTokenJSON,
	fillCollateralsData,
	getWNFTById,
} from '../fetchtokenwrapper';
import {
	decodeWrappedToken,
	WNFT,
	_AssetType,
	NFTorWNFT,
	CollateralItem,
	getNativeCollateral,
	encodeCollaterals,
	Lock,
	LockType,
	NFT
} from '../_types';
import { BigNumber, processSwarmUrl } from '../_utils';
import {
	checkContractType,
	createContract,
	isContractWNFTFromChain,
} from './common';
import { NetworkByID } from '../aptosUtils';
import {
	getERC20BalanceFromChain,
	//	makeERC20Allowance,
	//	makeERC20AllowanceMultisig
} from './erc20';
import config from '../../app.config.json';

import {
	//	checkApprovalERC1155,
	//	checkApprovalERC721,
	//	getBalanceERC1155,
	//	getERC721ByIdFromChain,
	getDAByIdFromChain,
	//	setApprovalERC1155,
	//	setApprovalERC1155Multisig,
	//	setApprovalERC721,
	//	setApprovalERC721Multisig,
	//	transferERC1155,
	//	transferERC1155Multisig,
	transferERC721,
	//	transferERC721Multisig
} from './nft';

import {
	Account,
	AccountAddress,
	Aptos,
	AptosConfig,
	Ed25519PrivateKey,
	Network,
	NetworkToNetworkName,
	NetworkToChainId,
	InputViewRequestData,
	AccountAddressInput,
} from '@aptos-labs/ts-sdk';

import { getDefaultWeb3 } from './web3provider';
import { log } from 'console';

export const getWNFTsOfContractFromChain721 = async (
	chainId: number,
	contractAddress: string,
	userAddress: string,
): Promise<Array<WNFT>> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_wnft721', contractAddress);
	const balance = await contract.methods.balanceOf(userAddress).call();

	let wnfts = [];

	for (let idx = balance - 1; idx >= 0; idx--) {
		const contractTokenId = await contract.methods
			.tokenOfOwnerByIndex(userAddress, idx)
			.call();

		let tokenUrl = '';
		let owner = '';
		let copies = new BigNumber(0);
		let totalSupply = new BigNumber(0);

		tokenUrl = await contract.methods.tokenURI(contractTokenId).call();
		owner = await contract.methods.ownerOf(contractTokenId).call();

		const wrappedToken = await contract.methods
			.wnftInfo(contractTokenId)
			.call();

		let wrappedTokenParsed = await decodeWrappedToken({
			inWNFT: wrappedToken,
			owner: owner,
			chainId: chainId,
			contractAddress: contractAddress,
			tokenId: contractTokenId,
			assetType: _AssetType.DA,
			amount: copies,
			totalSupply: totalSupply,
			tokenUrl: tokenUrl,
		});
		wrappedTokenParsed = await fillCollateralsData(wrappedTokenParsed);
		wnfts.push(wrappedTokenParsed);
	}

	return wnfts;
};

export const getWNFTsOfContractFromChain = async (
	chainId: number,
	collectionAddress: string,
	userAddress: string,
): Promise<Array<WNFT>> => {
	let aptosNetwork = NetworkByID(chainId);

	let aptosConfig = new AptosConfig({ network: aptosNetwork });

	let aptos = new Aptos(aptosConfig);
	let collectionAptosAddress = AccountAddress.fromString(collectionAddress);
	let userAptosAddress = AccountAddress.fromString(userAddress);
	let tokensData = await aptos.getAccountOwnedTokensFromCollectionAddress({
		accountAddress: userAptosAddress,
		collectionAddress: collectionAptosAddress,
	});
	console.log(tokensData);
/*	const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId == chainId });
	console.log('CHAIN_SPECIFIC_DATA', config.CHAIN_SPECIFIC_DATA);
	console.log('chainId', chainId);
	console.log('foundChain',foundChain);
	
	let wrapper = foundChain?.wrapperContract;
	let view_fumction = `${wrapper}::${'env_wrapper'}::${'get_FA_collateral'}`
*/
	let wnfts: Array<WNFT> = [];
	
	for (let idx = 0; idx < tokensData.length; idx++) {
/*		let token = getDAByIdFromChain(tokensData[idx].token_data_id, chainId);

		let collateral:Array<CollateralItem>=[];
		let payload: InputViewRequestData = {
		  function: (view_fumction as any),
		  functionArguments: [tokensData[idx].token_data_id],
		};
		//Colateral TODO
		try {
			let res = (await aptos.view<[{ inner: string }]>({ payload }))[0];
			//collateral = res.
			console.log(res);
			} catch (e) {
			console.log(e);
		}
//		res.current_token_data.token_uri

		let wrappedToken: WNFT = {
			...token,
			collateral: collateral,
			fees: [],
			royalties: [],
			locks: [],
			unWrapDestination: '',
			rules: {
				noUnwrap: false,
				noAddCollateral: false,
				noWrap: false,
				noTransfer: false,
			},
			collectedFees: new BigNumber(0),
			chainId: chainId,
			tokenId: tokensData[idx].token_data_id,
			assetType: -1,
			contractAddress: contractAddress,
		};
		//let wrappedTokenParsed = await decodeWrappedToken({todo

		//wrappedToken = await fillCollateralsData(wrappedToken);
*/
		let wnft = await getWNFTByIdFromChain(chainId,tokensData[idx].token_data_id,userAddress);
		wnfts.push(wnft);
	}

	/*	const web3 = await getDefaultWeb3(chainId);
	if ( !web3 ) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_wnft721', contractAddress);
	const balance = await contract.methods.balanceOf( userAddress ).call();
*/

	/*
	for (let idx = balance - 1; idx >= 0; idx--) {
		const contractTokenId = await contract.methods.tokenOfOwnerByIndex(userAddress, idx).call();

		let tokenUrl        = '';
		let owner           = '';
		let copies          = new BigNumber(0);
		let totalSupply     = new BigNumber(0);

		tokenUrl = await contract.methods.tokenURI(contractTokenId).call();
		owner    = await contract.methods.ownerOf(contractTokenId).call()

		const wrappedToken = await contract.methods.wnftInfo(contractTokenId).call();

		let wrappedTokenParsed = await decodeWrappedToken({
			inWNFT         : wrappedToken,
			owner          : owner,
			chainId        : chainId,
			contractAddress: contractAddress,
			tokenId        : contractTokenId,
			assetType      : _AssetType.ERC721,
			amount         : copies,
			totalSupply    : totalSupply,
			tokenUrl       : tokenUrl,
		});
		wrappedTokenParsed = await fillCollateralsData(wrappedTokenParsed);
		wnfts.push(wrappedTokenParsed);
	};
*/
	return wnfts;
};

export const getWNFTsOfContractFromChain721Batch = async (
	chainId: number,
	contractAddress: Array<string>,
	userAddress: string,
): Promise<Array<WNFT>> => {
	let out: Array<WNFT> = [];

	for (let idx = 0; idx < contractAddress.length; idx++) {
		const item = contractAddress[idx];

		const wnftsOfContract = await getWNFTsOfContractFromChain721(
			chainId,
			item,
			userAddress,
		);
		out = [...out, ...wnftsOfContract];
	}

	return out;
};

export const getBalance721FromChain = async (
	chainId: number,
	contractAddress: string,
	userAddress: string,
): Promise<BigNumber> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_wnft721', contractAddress);
	return new BigNumber(await contract.methods.balanceOf(userAddress).call());

};


export const getBalanceDAFromChain = async (
	chainId: number,
	contractAddress: string,
	userAddress: string,
): Promise<BigNumber> => {
	

	let aptosNetwork = NetworkByID(chainId);
	let aptosConfig = new AptosConfig({ network: aptosNetwork });
	let aptos = new Aptos(aptosConfig);

	let collectionAptosAddress = AccountAddress.fromString(contractAddress);
	let userAptosAddress = AccountAddress.fromString(userAddress);
	let tokensData = await aptos.getAccountOwnedTokensFromCollectionAddress({
		accountAddress: userAptosAddress,
		collectionAddress: collectionAptosAddress,
	});
	console.log(tokensData);

	return  new BigNumber(tokensData.length);
};

export const getBalance1155FromChain = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress: string,
): Promise<{ balance: BigNumber; totalSupply: BigNumber }> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, '_wnft1155', contractAddress);
	const balance = new BigNumber(
		await contract.methods.balanceOf(userAddress, tokenId).call(),
	);
	const totalSupply = new BigNumber(
		await contract.methods.totalSupply(userAddress, tokenId).call(),
	);
	return {
		balance,
		totalSupply,
	};
};

/*
export const getWNFTByIdFromChain = async (
	chainId: number,
	contractAddress: string,
	tokenId: string,
	userAddress?: string,
): Promise<WNFT> => {
	if (contractAddress === '') {
		throw new Error('Empty contract address');
	}
	if (tokenId === '') {
		throw new Error('Empty token id');
	}

	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contractType = await checkContractType(chainId, contractAddress);

	let contract;

	if (contractType === _AssetType.ERC721) {
		contract = await createContract(web3, '_wnft721', contractAddress);
	}
	if (contractType === _AssetType.ERC1155) {
		contract = await createContract(web3, '_wnft1155', contractAddress);
	}

	if (!contract) {
		throw new Error('Cannot check type of contract');
	}

	let tokenUrl = '';
	let owner = '';
	let copies = new BigNumber(0);
	let totalSupply = new BigNumber(0);

	if (contractType === _AssetType.ERC721) {
		tokenUrl = await contract.methods.tokenURI(tokenId).call();
		owner = await contract.methods.ownerOf(tokenId).call();
	}

	if (contractType === _AssetType.ERC1155) {
		tokenUrl = await contract.methods.uri(tokenId).call();
		if (userAddress) {
			try {
				copies = new BigNumber(
					await contract.methods.balanceOf(userAddress, tokenId).call(),
				);
			} catch (e) {
				console.log(`Cannot get amount of token ${tokenId}`);
			}
		}
		try {
			totalSupply = new BigNumber(
				await contract.methods.totalSupply(tokenId).call(),
			);
		} catch (e) {
			console.log(`Cannot get amount of token ${tokenId}`);
		}
	}

	const wrappedToken = await contract.methods.wnftInfo(tokenId).call();

	let wrappedTokenParsed = await decodeWrappedToken({
		inWNFT: wrappedToken,
		owner: owner,
		chainId: chainId,
		contractAddress: contractAddress,
		tokenId: tokenId,
		assetType: contractType,
		amount: copies,
		totalSupply: totalSupply,
		tokenUrl: tokenUrl,
	});
	wrappedTokenParsed = await fillCollateralsData(wrappedTokenParsed);

	let tokenJSONData: any = {};
	if (wrappedTokenParsed.tokenUrl) {
		tokenJSONData = await fetchTokenJSON(wrappedTokenParsed.tokenUrl);
	}

	return {
		...wrappedTokenParsed,
		...tokenJSONData,
	};
};*/


export const getWNFTByIdFromChain = async (
	chainId: number,
//	contractAddress: string,
	tokenId: string,
	userAddress: string,
): Promise<WNFT> => {
	if (tokenId === '') {
		throw new Error('Empty token id');
	}

	let aptosNetwork = NetworkByID(chainId);

	let aptosConfig = new AptosConfig({ network: aptosNetwork });

	let aptos = new Aptos(aptosConfig);

/*
	let tokenUrl = '';
	let owner = '';
	let copies = new BigNumber(0);
	let totalSupply = new BigNumber(0);
*/
	let token = await getDAByIdFromChain(tokenId, chainId, userAddress);

/*
	if (contractType === _AssetType.ERC721) {
		tokenUrl = await contract.methods.tokenURI(tokenId).call();
		owner = await contract.methods.ownerOf(tokenId).call();
	}

	if (contractType === _AssetType.ERC1155) {
		tokenUrl = await contract.methods.uri(tokenId).call();
		if (userAddress) {
			try {
				copies = new BigNumber(
					await contract.methods.balanceOf(userAddress, tokenId).call(),
				);
			} catch (e) {
				console.log(`Cannot get amount of token ${tokenId}`);
			}
		}
		try {
			totalSupply = new BigNumber(
				await contract.methods.totalSupply(tokenId).call(),
			);
		} catch (e) {
			console.log(`Cannot get amount of token ${tokenId}`);
		}
	}
*/
//	const wrappedToken = await contract.methods.wnftInfo(tokenId).call();

//Get Colateral TODO
		let contractAddress = token.contractAddress;
		const foundChain = config.CHAIN_SPECIFIC_DATA.find((item) => { return item.chainId == chainId });
		/*console.log('CHAIN_SPECIFIC_DATA', config.CHAIN_SPECIFIC_DATA);
		console.log('chainId', chainId);
		console.log('foundChain',foundChain);
		*/
		let wrapper = foundChain?.wrapperContract;
		let view_fumction = `${wrapper}::${'env_wrapper'}::${'get_FA_collateral'}`
		let collateral:Array<CollateralItem>=[];
		let payload: InputViewRequestData = {
		function: (view_fumction as any),
		functionArguments: [tokenId],
		};
		try {
			let res = (await aptos.view<[{ inner: string }]>({ payload }))[0];
			//collateral = res.;
			for (let idx = 0; idx < (res as any).length; idx++) {

				collateral.push({assetType:_AssetType.FA,
					contractAddress:(res as any)[idx].metadata_address,
					amount: new BigNumber((res as any)[idx].amount)
				});
			}
			console.log('get_FA_collateral',res);
			} catch (e) {
			console.log(e);
		}

		//get original and locks token

		view_fumction = `${wrapper}::${'env_wrapper'}::${'get_wrapped_token'}`
		let original_token={};
		let locks: Array<Lock>=[];
		payload = {
		function: (view_fumction as any),
		functionArguments: [tokenId],
		};
		let originalTokenInfo:NFT | undefined = undefined; 
//		console.log('get_wrapped_token tokenId',tokenId);
//		console.log('get_wrapped_token token ',tokenId, token);

		try {
			let res0 = await aptos.view<[{ inner: string }]>({ payload });
//			console.log('get_wrapped_token res0',res0);

			let res = (res0)[0];
		//	original_token=res.;
			originalTokenInfo = await getDAByIdFromChain((res as any).addr, chainId, userAddress);
			if ((res as any).backed_value>0) {
					collateral.push({assetType:_AssetType.native,
						contractAddress:'0x0000000000000000000000000000000000000000',
						amount: new BigNumber((res as any).backed_value)
					});
				}
			if ((res as any).unwrap_after>0) {
				locks.push({
					lockType: LockType.time,
					param   : new BigNumber((res as any).unwrap_after)
				});
			}
		//	locks=res.;
//		console.log('get_wrapped_token restokenId',tokenId);
		console.log('get_wrapped_token',tokenId, res);
			} catch (e) {
//				console.log('get_wrapped_token error tokenId',tokenId);
				console.log('get_wrapped_token error',tokenId, e);

			console.log(e);
		}


/*
	let wrappedTokenParsed = await decodeWrappedToken({
		inWNFT: wrappedToken,
		owner: owner,
		chainId: chainId,
		contractAddress: contractAddress,
		tokenId: tokenId,
		assetType: contractType,
		amount: copies,
		totalSupply: totalSupply,
		tokenUrl: tokenUrl,
	});
	wrappedTokenParsed = await fillCollateralsData(wrappedTokenParsed);

	let tokenJSONData: any = {};
	if (wrappedTokenParsed.tokenUrl) {
		tokenJSONData = await fetchTokenJSON(wrappedTokenParsed.tokenUrl);
	}

	return {
		...wrappedTokenParsed,
		...tokenJSONData,
	};
	*/
	const tokenURL = token.tokenUrl?token.tokenUrl:'';
	const wnft:WNFT = {
		...token,
		collateral: collateral,
		fees: [],
		royalties: [],
		locks: locks,
		unWrapDestination: '',
		rules: {
			noUnwrap: false,
			noAddCollateral: false,
			noWrap: false,
			noTransfer: false,
		},
		collectedFees: new BigNumber(0),
		chainId: chainId,
		tokenId: tokenId,
		assetType: -1,
		contractAddress: contractAddress,
		tokenUrl:processSwarmUrl(tokenURL),
		tokenUrlRaw:tokenURL,
		originalTokenInfo: originalTokenInfo
	};
	return wnft;
};
/**
 * Transfer ERC721 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {WNFT} token - token-object which should be transfered
 * @param {string} userAddress - user who transfer WNFT
 * @param {string} addressTo - address-receiver of WNFT
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const transferWNFT = async (
	web3: Web3,
	token: NFTorWNFT,
	amount: BigNumber,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {
	if (token.rules && token.rules.noTransfer) {
		throw new Error('Cannot transfer token due to the rule');
	}

	const chainId = await web3.eth.getChainId();
	if (token.fees && token.fees.length) {
		const balance = await getERC20BalanceFromChain(
			chainId,
			token.fees[0].token,
			userAddress,
			token.contractAddress,
		);
		if (balance.balance.lt(token.fees[0].value)) {
			throw new Error('Not enough tokens for fee');
		} /*
		if ( balance.allowance?.amount.lt(token.fees[0].value) ) {
			await makeERC20Allowance(
				web3,
				token.fees[0].token,
				userAddress,
				token.fees[0].value,
				token.contractAddress
			)
		}*/
	}
	/*
	if ( token.assetType === _AssetType.ERC1155 ) {
		return transferERC1155(
			web3,
			token.contractAddress,
			token.tokenId,
			amount || new BigNumber(1),
			userAddress,
			addressTo
		)
	}
*/
	return transferERC721(
		web3,
		token.contractAddress,
		token.tokenId,
		userAddress,
		addressTo,
	);
};
/**
 * Transfer ERC721 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {WNFT} token - token-object which should be transfered
 * @param {string} userAddress - user who transfer WNFT
 * @param {string} addressTo - address-receiver of WNFT
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */ /*
export const transferWNFTMultisig = async (
	web3: Web3,
	token: NFTorWNFT,
	amount: BigNumber,
	userAddress: string,
	addressTo: string,
): Promise<PromiEvent<Contract>> => {

	if ( token.rules && token.rules.noTransfer ) {
		throw new Error('Cannot transfer token due to the rule')
	}

	const chainId = await web3.eth.getChainId();
	if ( token.fees && token.fees.length ) {
		const balance = await getERC20BalanceFromChain(
			chainId,
			token.fees[0].token,
			userAddress,
			token.contractAddress
		);
		if ( balance.balance.lt(token.fees[0].value) ) {
			throw new Error('Not enough tokens for fee');
		}
		if ( balance.allowance?.amount.lt(token.fees[0].value) ) {
			await makeERC20AllowanceMultisig(
				web3,
				token.fees[0].token,
				userAddress,
				token.fees[0].value,
				token.contractAddress
			)
		}
	}

	if ( token.assetType === _AssetType.ERC1155 ) {
		return transferERC1155Multisig(
			web3,
			token.contractAddress,
			token.tokenId,
			amount || new BigNumber(1),
			userAddress,
			addressTo
		)
	}

	return transferERC721Multisig(
		web3,
		token.contractAddress,
		token.tokenId,
		userAddress,
		addressTo
	)
}*/

/**
 * Transfer ERC721 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {WNFT} token - token-object which should be transfered
 * @param {string} userAddress - user who transfer WNFT
 * @param {string} addressTo - address-receiver of WNFT
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */
export const addFAValueToWNFT = async (
	chainId: number,
	token: WNFT,
	collateral_meta_address: string,
	userAddress: string,
	signAndSubmitTransaction:Function,
	value:number
): Promise<any> => {
//	const chainId = await web3.eth.getChainId();
/*
	const wrapperAddress =
		(await getWNFTWrapperContract(chainId, token.contractAddress)) ||
		token.contractAddress;*/
		const wrapperAddress = token.contractAddress;
/*
	const erc20collateral = collateral.filter((item) => {
		return item.assetType === _AssetType.ERC20;
	});
	
	for (let idx = 0; idx < erc20collateral.length; idx++) {
		const item = erc20collateral[idx];
		if (!item.amount) {
			continue;
		}

		const balance = await getERC20BalanceFromChain(
			chainId,
			item.contractAddress,
			userAddress,
			wrapperAddress,
		);
		if (balance.balance.lt(item.amount)) {
			throw new Error('Not enough balance');
		}
		if ( balance.allowance?.amount.lt(item.amount) ) {
			await makeERC20Allowance(web3, item.contractAddress, userAddress, item.amount, wrapperAddress);
		}
	}*/
	/*
	const erc721collateral = collateral.filter((item) => {
		return item.assetType === _AssetType.ERC721;
	});
	for (let idx = 0; idx < erc721collateral.length; idx++) {
		const item = erc721collateral[idx];

		//		const nft = await getERC721ByIdFromChain(chainId, item.contractAddress, item.tokenId || '');
		const nft = await getDAByIdFromChain(item.tokenId || '', chainId);
		if (nft.owner?.toLowerCase() !== userAddress.toLowerCase()) {
			throw new Error('User is not owner');
		}
		
		const approve = await checkApprovalERC721(chainId, item.contractAddress, item.tokenId || '', userAddress, wrapperAddress);
		if ( !approve ) {
			await setApprovalERC721(web3, item.contractAddress, item.tokenId || '', userAddress, wrapperAddress);
		}
	}
*/
	/*
	const erc1155collateral = collateral.filter((item) => { return item.assetType === _AssetType.ERC721 });
	for (let idx = 0; idx < erc1155collateral.length; idx++) {
		const item = erc1155collateral[idx];
		if ( !item.amount ) { continue; }

		const balance = await getBalanceERC1155(chainId, item.contractAddress, userAddress, item.tokenId || '');
		if ( balance.lt(item.amount) ) { throw new Error(`Not enough balance of 1155 token: ${item.contractAddress}: ${item.tokenId}`) }

		const approve = await checkApprovalERC1155(chainId, item.contractAddress, userAddress, wrapperAddress);
		if ( !approve ) {
			await setApprovalERC1155(web3, item.contractAddress, userAddress, wrapperAddress);
		}
	}

	for (let idx = 0; idx < collateral.length; idx++) {
		const item = collateral[idx];

		if (!item.tokenId) {
			continue;
		}
		if (
			!(await isContractWNFTFromChain(
				chainId,
				item.contractAddress,
				item.tokenId,
			))
		) {
			continue;
		}

		const token = await getWNFTById(
			chainId,
			item.contractAddress,
			item.tokenId,
		);
		if (!token || !token.fees || !token.fees.length || !token.fees[0]) {
			continue;
		}

		const fee = token.fees[0];
		const balance = await getERC20BalanceFromChain(
			chainId,
			fee.token,
			userAddress,
			wrapperAddress,
		);
		if (balance.balance.lt(fee.value)) {
			throw new Error(
				`Not enough balance for transfer fee. Fee token: ${fee.token}, fee value: ${fee.value}. Token to transfer: ${item.contractAddress}: ${item.tokenId}`,
			);
		}
		if ( balance.allowance?.amount.lt(fee.value) ) {
			await makeERC20Allowance(web3, item.contractAddress, userAddress, fee.value, wrapperAddress);
		}
	}

	const nativeCollateral = getNativeCollateral(collateral);
*/

		let aptosNetwork = NetworkByID(chainId);
		let aptosConfig = new AptosConfig({ network: aptosNetwork });
		let aptos = new Aptos(aptosConfig);
		let collectionAptosAddress = AccountAddress.fromString(collateral_meta_address);
		let userAptosAddress = AccountAddress.fromString(userAddress);
	
		let tokensData = await aptos.getAccountOwnedTokensFromCollectionAddress({
			accountAddress: userAptosAddress,
			collectionAddress: collectionAptosAddress,
		});

/*variant		let data = await aptos.getAccountCoinsData({accountAddress: wrapper})
		let before_fungable_token_balance = (data as any)[1].amount;
	   
*/	  
		console.log(tokensData);
		let balance = new BigNumber((tokensData as any).value);
		if (balance.lt( new BigNumber(value))) {
			throw new Error(
				`Not enough balance for token (meta_address ${collateral_meta_address})`,
			);
		}

	try {

		const response = await signAndSubmitTransaction({
			sender: userAddress,
			data: {
			  function: `${token.contractAddress}::env_wrapper::add_fa_collateral`,
			  typeArguments: [],
			  functionArguments: [token.tokenId, collateral_meta_address, value ],
			},
		  });
/*
		const response = await signAndSubmitTransaction({
		type: "entry_function_payload",
		function: wrapperAddress+"::env_wrapper::add_fa_collateral",
		arguments: [token.tokenId, collateral_meta_address, value ],
		type_arguments: [],
	  });*/
	  console.log('response', response);
	return response;
	} catch (error) {
		console.log(error);
		throw new Error(
			`Error while added colateral ${error})`,
		);
	}
/*
	const wrapperContract = await createContract(web3, 'wrapper', wrapperAddress);
	const tx = wrapperContract.methods.addCollateral(
		token.contractAddress,
		token.tokenId,
		encodeCollaterals(collateral),
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral });
	} catch (e) {
		throw e;
	}

	return tx.send({
		from: userAddress,
		value: nativeCollateral,
		maxPriorityFeePerGas: null,
		maxFeePerGas: null,
	});
	*/
};


export const addNativeValueToWNFT = async (
	chainId: number,
	token: WNFT,
	userAddress: string,
	signAndSubmitTransaction:Function,
	value:number
): Promise<any> => {
//	const chainId = await web3.eth.getChainId();
console.log('addNativeValueToWNFT start 2');

console.log('signAndSubmitTransaction 2', signAndSubmitTransaction);

/*	const wrapperAddress =

		(await getWNFTWrapperContract(chainId, token.contractAddress)) ||
		token.contractAddress;*/
		const wrapperAddress = token.contractAddress;
	console.log('addNativeValueToWNFT wrapperAddress',wrapperAddress);
	

	console.log('addNativeValueToWNFT chainId',chainId);
		let aptosNetwork = NetworkByID(chainId);
		let aptosConfig = new AptosConfig({ network: aptosNetwork });
		let aptos = new Aptos(aptosConfig);
		let userAptosAddress = AccountAddress.fromString(userAddress);
//		console.log('addNativeValueToWNFT userAddress', userAddress);
	
		let coinsData = await  aptos.getAccountCoinsData({
			accountAddress: userAddress,
		});
		console.log('coinsData', coinsData);


		let amount = await aptos.getAccountAPTAmount({
			accountAddress: userAptosAddress
		  });
		  console.log('addNativeValueToWNFT amount',amount);

		  amount = await aptos.getAccountAPTAmount({
			accountAddress: userAddress
		  });
		  console.log('addNativeValueToWNFT amount1',amount);
		  let balance = new BigNumber(amount);
		  console.log('addNativeValueToWNFT balance',balance);
		  console.log('addNativeValueToWNFT amount',amount);
		  console.log('addNativeValueToWNFT value',value);
		  console.log('addNativeValueToWNFT balance.lt( new BigNumber(value))', balance.lt( new BigNumber(value)));
		  if (balance.lt( new BigNumber(value))) {
			  throw new Error(
				  `Not enough native coin balance`,
			  );
		  }
  
		

/*
const response = await signAndSubmitTransaction({
	type: "entry_function_payload",
	function: wrapperAddress+"::env_wrapper::add_native_collateral",
	arguments: [token.tokenId, value ],
	type_arguments: [],
  });
  console.log('response', response);*/
	try {/*
		const response = await signAndSubmitTransaction({
		type: "entry_function_payload",
		function: wrapperAddress+"::env_wrapper::add_native_collateral",
		arguments: [token.tokenId, value ],
		type_arguments: ['0x1::aptos_coin::AptosCoin'],
	  });
	  console.log('response', response);
	return response;
*//*
	const response = await signAndSubmitTransaction({
		type: "entry_function_payload",
		function: wrapperAddress+"::env_wrapper::wrap_with_mint",
		arguments: [0, 0],
		type_arguments: [],
	  });
	  console.log('response', response);
*/
	  const response = await signAndSubmitTransaction({
		sender: userAddress,
		data: {
		  function: `${token.contractAddress}::env_wrapper::add_native_collateral`,
		  typeArguments: [],
		  functionArguments: [token.tokenId, value ],
		},
	  });
	  console.log('response', response);
	  return response;

	} catch (error) {
		console.log(error);
		throw new Error(
			`Error while added colateral ${error}`,
		);
	}

};



export const unwrapDA = async (
	token: WNFT,
	userAddress: string,
	signAndSubmitTransaction:Function,
): Promise<any> => {

	try {
	  const response = await signAndSubmitTransaction({
		sender: userAddress,
		data: {
		  function: `${token.contractAddress}::env_wrapper::unwrap`,
		  typeArguments: [],
		  functionArguments: [token.tokenId],
		},
	  });
	  return response;

	} catch (error) {
		console.log(error);
		throw new Error(
			`Error while uwrap token ${error}`,
		);
	}

};
/*
export const addFAValueToWNFT = async (
	chainId: number,
	token: WNFT,
	collateral: Array<CollateralItem>,
	userAddress: string,
	signAndSubmitTransaction:Function,
): Promise<PromiEvent<Contract>> => {
//	const chainId = await web3.eth.getChainId();

	const wrapperAddress =
		(await getWNFTWrapperContract(chainId, token.contractAddress)) ||
		token.contractAddress;

	const erc20collateral = collateral.filter((item) => {
		return item.assetType === _AssetType.ERC20;
	});
	for (let idx = 0; idx < erc20collateral.length; idx++) {
		const item = erc20collateral[idx];
		if (!item.amount) {
			continue;
		}

		const balance = await getERC20BalanceFromChain(
			chainId,
			item.contractAddress,
			userAddress,
			wrapperAddress,
		);
		if (balance.balance.lt(item.amount)) {
			throw new Error('Not enough balance');
		}
		if ( balance.allowance?.amount.lt(item.amount) ) {
			await makeERC20Allowance(web3, item.contractAddress, userAddress, item.amount, wrapperAddress);
		}
	}
	
	const erc721collateral = collateral.filter((item) => {
		return item.assetType === _AssetType.ERC721;
	});
	for (let idx = 0; idx < erc721collateral.length; idx++) {
		const item = erc721collateral[idx];

		//		const nft = await getERC721ByIdFromChain(chainId, item.contractAddress, item.tokenId || '');
		const nft = await getDAByIdFromChain(item.tokenId || '', chainId);
		if (nft.owner?.toLowerCase() !== userAddress.toLowerCase()) {
			throw new Error('User is not owner');
		}
		
		const approve = await checkApprovalERC721(chainId, item.contractAddress, item.tokenId || '', userAddress, wrapperAddress);
		if ( !approve ) {
			await setApprovalERC721(web3, item.contractAddress, item.tokenId || '', userAddress, wrapperAddress);
		}
	}

	
	const erc1155collateral = collateral.filter((item) => { return item.assetType === _AssetType.ERC721 });
	for (let idx = 0; idx < erc1155collateral.length; idx++) {
		const item = erc1155collateral[idx];
		if ( !item.amount ) { continue; }

		const balance = await getBalanceERC1155(chainId, item.contractAddress, userAddress, item.tokenId || '');
		if ( balance.lt(item.amount) ) { throw new Error(`Not enough balance of 1155 token: ${item.contractAddress}: ${item.tokenId}`) }

		const approve = await checkApprovalERC1155(chainId, item.contractAddress, userAddress, wrapperAddress);
		if ( !approve ) {
			await setApprovalERC1155(web3, item.contractAddress, userAddress, wrapperAddress);
		}
	}

	for (let idx = 0; idx < collateral.length; idx++) {
		const item = collateral[idx];

		if (!item.tokenId) {
			continue;
		}
		if (
			!(await isContractWNFTFromChain(
				chainId,
				item.contractAddress,
				item.tokenId,
			))
		) {
			continue;
		}

		const token = await getWNFTById(
			chainId,
			item.contractAddress,
			item.tokenId,
		);
		if (!token || !token.fees || !token.fees.length || !token.fees[0]) {
			continue;
		}

		const fee = token.fees[0];
		const balance = await getERC20BalanceFromChain(
			chainId,
			fee.token,
			userAddress,
			wrapperAddress,
		);
		if (balance.balance.lt(fee.value)) {
			throw new Error(
				`Not enough balance for transfer fee. Fee token: ${fee.token}, fee value: ${fee.value}. Token to transfer: ${item.contractAddress}: ${item.tokenId}`,
			);
		}
		if ( balance.allowance?.amount.lt(fee.value) ) {
			await makeERC20Allowance(web3, item.contractAddress, userAddress, fee.value, wrapperAddress);
		}
	}

	const nativeCollateral = getNativeCollateral(collateral);

	try {
		const response = await signAndSubmitTransaction({
		type: "entry_function_payload",
		function: wrapperAddress+"::env_wrapper::add_fa_collateral",
		arguments: [token.tokenId, token.contractAddress, value ],
		type_arguments: [],
	  });
	  console.log('response', response);
	return response;
	} catch (error) {
		console.log(error);
		return undefined;
	}

	const wrapperContract = await createContract(web3, 'wrapper', wrapperAddress);
	const tx = wrapperContract.methods.addCollateral(
		token.contractAddress,
		token.tokenId,
		encodeCollaterals(collateral),
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral });
	} catch (e) {
		throw e;
	}

	return tx.send({
		from: userAddress,
		value: nativeCollateral,
		maxPriorityFeePerGas: null,
		maxFeePerGas: null,
	});
	
};*/
/**
 * MULTISIG
 * Transfer ERC721 token to other address
 * @param {Web3} web3 - Web3-object which used for blockchain calls
 * @param {WNFT} token - token-object which should be transfered
 * @param {string} userAddress - user who transfer WNFT
 * @param {string} addressTo - address-receiver of WNFT
 * @returns {Promise<PromiEvent<Contract>>}> Promise of sent function (can be listened in outer function)
 */ /*
export const addValueToWNFTMultisig = async (
	web3: Web3,
	token: WNFT,
	collateral: Array<CollateralItem>,
	userAddress: string,
): Promise<PromiEvent<Contract>> => {

	const chainId = await web3.eth.getChainId();

	const wrapperAddress = await getWNFTWrapperContract(chainId, token.contractAddress) || token.contractAddress;

	const erc20collateral = collateral.filter((item) => { return item.assetType === _AssetType.ERC20 });
	for (let idx = 0; idx < erc20collateral.length; idx++) {
		const item = erc20collateral[idx];
		if ( !item.amount ) { continue; }

		const balance = await getERC20BalanceFromChain(chainId, item.contractAddress, userAddress, wrapperAddress);
		if ( balance.balance.lt(item.amount) ) { throw new Error('Not enough balance') }
		if ( balance.allowance?.amount.lt(item.amount) ) {
			await makeERC20AllowanceMultisig(web3, item.contractAddress, userAddress, item.amount, wrapperAddress);
		}
	}
	const erc721collateral = collateral.filter((item) => { return item.assetType === _AssetType.ERC721 });
	for (let idx = 0; idx < erc721collateral.length; idx++) {
		const item = erc721collateral[idx];

		const nft = await getERC721ByIdFromChain(chainId, item.contractAddress, item.tokenId || '');
		if ( nft.owner?.toLowerCase() !== userAddress.toLowerCase() ) { throw new Error('User is not owner') }

		const approve = await checkApprovalERC721(chainId, item.contractAddress, item.tokenId || '', userAddress, wrapperAddress);
		if ( !approve ) {
			await setApprovalERC721Multisig(web3, item.contractAddress, item.tokenId || '', userAddress, wrapperAddress);
		}
	}

	const erc1155collateral = collateral.filter((item) => { return item.assetType === _AssetType.ERC721 });
	for (let idx = 0; idx < erc1155collateral.length; idx++) {
		const item = erc1155collateral[idx];
		if ( !item.amount ) { continue; }

		const balance = await getBalanceERC1155(chainId, item.contractAddress, userAddress, item.tokenId || '');
		if ( balance.lt(item.amount) ) { throw new Error(`Not enough balance of 1155 token: ${item.contractAddress}: ${item.tokenId}`) }

		const approve = await checkApprovalERC1155(chainId, item.contractAddress, userAddress, wrapperAddress);
		if ( !approve ) {
			await setApprovalERC1155Multisig(web3, item.contractAddress, userAddress, wrapperAddress);
		}
	}

	for (let idx = 0; idx < collateral.length; idx++) {
		const item = collateral[idx];

		if ( !item.tokenId ) { continue; }
		if ( !(await isContractWNFTFromChain(chainId, item.contractAddress, item.tokenId)) ) { continue; }

		const token = await getWNFTById(chainId, item.contractAddress, item.tokenId);
		if ( !token || !token.fees || !token.fees.length || !token.fees[0] ) { continue; }

		const fee = token.fees[0];
		const balance = await getERC20BalanceFromChain(chainId, fee.token, userAddress, wrapperAddress);
		if ( balance.balance.lt(fee.value) ) { throw new Error(`Not enough balance for transfer fee. Fee token: ${fee.token}, fee value: ${fee.value}. Token to transfer: ${item.contractAddress}: ${item.tokenId}`) }
		if ( balance.allowance?.amount.lt(fee.value) ) {
			await makeERC20AllowanceMultisig(web3, item.contractAddress, userAddress, fee.value, wrapperAddress);
		}
	}

	const nativeCollateral = getNativeCollateral(collateral);
	const wrapperContract = await createContract(web3, 'wrapper', wrapperAddress);
	const tx = wrapperContract.methods.addCollateral(token.contractAddress, token.tokenId, encodeCollaterals(collateral));

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral });
	} catch(e) {
		throw e;
	}

	return new Promise((res, rej) => {
		tx.send({ from: userAddress, value: nativeCollateral, maxPriorityFeePerGas: null, maxFeePerGas: null }, (err: any, data: any) => {
			if ( err ) { rej(err); }
			res(data);
		});
	});
}
*/
export const getWrapperTechToken = async (
	chainId: number,
	contractAddress: string,
): Promise<string> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}

	const contract = await createContract(web3, 'wrapper', contractAddress);
	return await contract.methods.protocolTechToken().call();
};
export const getWNFTWrapperContract = async (
	chainId: number,
	contractAddress: string,
): Promise<string> => {
	/*TODO
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}

	const contractType = await checkContractType(chainId, contractAddress);
	if (contractType === _AssetType.DA) {
		const contract = await createContract(web3, '_wnft1155', contractAddress);
		return await contract.methods.wrapper().call();
	} else {
		const contract = await createContract(web3, '_wnft721', contractAddress);
		return await contract.methods.wrapperMinter().call();
	}
	*/
	return ''
};
export const getWNFTv0TransferModel = async (
	chainId: number,
	wrapperAddress: string,
	tokenAddress: string,
): Promise<string> => {
	let transferModel = wrapperAddress;

	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		return transferModel;
	}
	const WNFTv0Wrapper = await createContract(web3, 'wnftv0', wrapperAddress);
	try {
		transferModel = (
			await WNFTv0Wrapper.methods.partnersTokenList(tokenAddress).call()
		).transferFeeModel;
	} catch (ignored) {}

	return transferModel;
};
export const getMaxCollateralSlots = async (
	chainId: number,
	wrapperAddress: string,
): Promise<number> => {
/*	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		return 0;
	}

	const wrapperContract = await createContract(web3, 'wrapper', wrapperAddress);
	return wrapperContract.methods.MAX_COLLATERAL_SLOTS().call();
*/
	return 25;
};
export const getWrapperOfBatchWorker = async (
	chainId: number,
	batchWorkerAddress: string,
): Promise<string> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		return '';
	}

	const batchWorkerContract = await createContract(
		web3,
		'batchworker',
		batchWorkerAddress,
	);
	return batchWorkerContract.methods.trustedWrapper().call();
};
