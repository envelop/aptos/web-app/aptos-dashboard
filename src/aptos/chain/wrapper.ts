import Web3 from 'web3';
import { BigNumber } from '../_utils';
import {
	CollateralItem,
	Fee,
	Lock,
	Royalty,
	Rules,
	_Asset,
	_AssetItem,
	_AssetType,
	_Fee,
	_Lock,
	_Royalty,
	decodeAssetTypeFromIndex,
	encodeCollaterals,
	encodeFees,
	encodeLocks,
	encodeRoyalties,
	encodeRules,
	getNativeCollateral,
} from '../_types';
import { createContract, getUserAddress } from './common';
import { getDefaultWeb3 } from './web3provider';
import { NetworkByID } from '../aptosUtils';
import {
	Account,
	AccountAddress,
	Aptos,
	AptosConfig,
	Ed25519PrivateKey,
	Network,
	NetworkToNetworkName,
	NetworkToChainId,
	InputViewRequestData,
	AccountAddressInput,
} from '@aptos-labs/ts-sdk';
import { isEmptyBindingElement } from 'typescript';

export const getMaxCollaterals = async (
	chainId: number,
	wrapperAddress: string,
): Promise<number> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);

	let slots = 25;
	try {
		slots = parseInt(await contract.methods.MAX_COLLATERAL_SLOTS().call());
	} catch (e: any) {
		throw e;
	}

	return slots;
};
export const getWrapperWhitelist = async (
	chainId: number,
	wrapperAddress: string,
): Promise<string | undefined> => {
/*	let aptosNetwork = NetworkByID(chainId);
	let aptosConfig = new AptosConfig({ network: aptosNetwork });
	let aptos = new Aptos(aptosConfig);
*/	return undefined;
	/*
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'wrapper', wrapperAddress);
	try {
		const whitelist = await contract.methods.protocolWhiteList().call();
		if (
			!whitelist ||
			whitelist === '' ||
			whitelist === '0x0000000000000000000000000000000000000000'
		) {
			return undefined;
		}
		return whitelist;
	} catch (e: any) {
		throw e;
	}*/
};
export const getWhitelist = async (
	chainId: number,
	whitelistAddress: string,
): Promise<Array<_Asset>> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const whitelist = await contract.methods.getWLAddresses().call();
		return whitelist.map(
			(item: { contractAddress: string; assetType: string }) => {
				return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) };
			},
		);
	} catch (e: any) {
		throw e;
	}
};
export const getBlacklist = async (
	chainId: number,
	whitelistAddress: string,
): Promise<Array<_Asset>> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		const blacklist = await contract.methods.getBLAddresses().call();
		return blacklist.map(
			(item: { contractAddress: string; assetType: string }) => {
				return { ...item, assetType: decodeAssetTypeFromIndex(item.assetType) };
			},
		);
	} catch (e: any) {
		throw e;
	}
};
export const isEnabledForCollateral = async (
	chainId: number,
	whitelistAddress: string,
	tokenAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForCollateral(tokenAddress).call();
	} catch (e: any) {
		throw e;
	}
};

export const getEnabledForCollateral = async (
	chainId: number,
	whitelistContractAddress: string,
): Promise<Array<string>> => {
	let ret:Array<string> = [];
	let aptosNetwork = NetworkByID(chainId);
	let aptosConfig = new AptosConfig({ network: aptosNetwork });
	let aptos = new Aptos(aptosConfig);


	let view_fumction = `${whitelistContractAddress}::${'env_wrapper'}::${'get_partners_token_list'}`
	let payload: InputViewRequestData = {
	function: (view_fumction as any),
	functionArguments: [],
	};
	try {
		let res = (await aptos.view<[{ inner: string }]>({ payload }))[0];
		console.log('get_partners_token_list',res);

		ret = (res as any).data.map((item: { value: { enabled_for_collateral: boolean; }; key: string; })=>{
			console.log('get_partners_token_list item',item);

			if (item.value.enabled_for_collateral) {return item.key}
		}); 
		} catch (e) {
		console.log(e);
		}

		console.log(ret);
		
	return ret
};
export const isEnabledForFee = async (
	chainId: number,
	whitelistAddress: string,
	tokenAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods.enabledForFee(tokenAddress).call();
	} catch (e: any) {
		throw e;
	}
};
export const isEnabledRemoveFromCollateral = async (
	chainId: number,
	whitelistAddress: string,
	tokenAddress: string,
): Promise<boolean> => {
	const web3 = await getDefaultWeb3(chainId);
	if (!web3) {
		throw new Error('Cannot connect to blockchain');
	}
	const contract = await createContract(web3, 'whitelist', whitelistAddress);
	try {
		return await contract.methods
			.enabledRemoveFromCollateral(tokenAddress)
			.call();
	} catch (e: any) {
		throw e;
	}
};

export type _INData = {
	inAsset: _AssetItem;
	unWrapDestinition: string; // fallback for old contracts; will be deprecated
	unWrapDestination: string;
	fees: Array<_Fee>;
	locks: Array<_Lock>;
	royalties: Array<_Royalty>;
	outType: _AssetType;
	outBalance: string;
	rules: string;
};
export type WrapTransactionArgs = {
	_inData: _INData;
	_collateral: Array<_AssetItem>;
	_wrappFor: string;
};
/*
export const encodeINData = (params: {
	originalToken         : {
		assetType: _AssetType,
		contractAddress: string,
		tokenId: string,
		amount: BigNumber,
	},
	unwrapDestination     : string,
	fees                  : Array<Fee>,
	locks                 : Array<Lock>,
	royalties             : Array<Royalty>,
	rules                 : Rules,
	outType               : _AssetType,
	outBalance            : number,
	wrapperContractAddress: string,
}): _INData => {
	return {
		inAsset          : {
			asset: {
				assetType: params.originalToken.assetType,
				contractAddress: params.originalToken.contractAddress || '0x0000000000000000000000000000000000000000',
			},
			tokenId: params.originalToken.tokenId || '0',
			amount: params.originalToken.amount ? params.originalToken.amount.toString() : '0',
		},
		unWrapDestinition: params.unwrapDestination,
		unWrapDestination: params.unwrapDestination,
		fees             : encodeFees(params.fees),
		locks            : encodeLocks(params.locks),
		royalties        : encodeRoyalties(params.royalties, params.wrapperContractAddress),
		outType          : params.outType,
		outBalance       : params.outBalance.toString(),
		rules            : encodeRules(params.rules),
	}
}
export const encodeWrapArguments = (params: {
	originalToken         : {
		assetType: _AssetType,
		contractAddress: string,
		tokenId: string,
		amount: BigNumber,
	},
	unwrapDestination     : string,
	fees                  : Array<Fee>,
	locks                 : Array<Lock>,
	royalties             : Array<Royalty>,
	rules                 : Rules,
	outType               : _AssetType,
	outBalance            : number,
	collaterals           : Array<CollateralItem>,
	wrapFor               : string,
	wrapperContractAddress: string,
}): WrapTransactionArgs => {

	let transferToken = undefined;
	if ( params.fees.length ) {
		transferToken = params.fees[0].token;
	}

	return {
		_inData: encodeINData({ ...params }),
		_collateral: encodeCollaterals(params.collaterals, transferToken),
		_wrappFor: params.wrapFor,
	}
}
*/
/*
export const wrapToken = async (
	web3: Web3,
	wrapperContract: string,
	params: {
		originalToken         : {
			assetType: _AssetType,
			contractAddress: string,
			tokenId: string,
			amount: BigNumber,
		},
		unwrapDestination     : string,
		fees                  : Array<Fee>,
		locks                 : Array<Lock>,
		royalties             : Array<Royalty>,
		rules                 : Rules,
		outType               : _AssetType,
		outBalance            : number,
		collaterals           : Array<CollateralItem>,
		wrapFor               : string,
	}
) => {

	const userAddress = await getUserAddress(web3);
	if ( !userAddress ) { return; }

	const contract = await createContract(web3, 'wrapper', wrapperContract);

	const nativeCollateral = getNativeCollateral(params.collaterals);
	const encodedParams = encodeWrapArguments({ ...params, wrapperContractAddress: wrapperContract });
	const tx = contract.methods.wrap(
		encodedParams._inData,
		encodedParams._collateral,
		encodedParams._wrappFor,
	);

	try {
		await tx.estimateGas({ from: userAddress, value: nativeCollateral })
	} catch(e) {
		throw e;
	}

	return tx.send({ from: userAddress, value: nativeCollateral, maxPriorityFeePerGas: null, maxFeePerGas: null });
}*/
