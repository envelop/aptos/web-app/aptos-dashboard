import { useContext } from 'react';
import {
	BigNumber,
	CollateralItem,
	ERC20Type,
	_AssetType,
} from '../../aptos';
import { ERC20Context, Web3Context } from '../../dispatchers';
import default_icon from '../../static/pics/coins/_default.svg';
import default_nft from '../../static/pics/coins/_default_nft.svg';

type CoinIconViewerProps = {
	tokens: Array<CollateralItem>;

	onClick?: Function;
	onMouseEnter?: Function;
	onMouseLeave?: Function;
};

export default function CoinIconViewer(props: CoinIconViewerProps) {
	const { tokens } = props;

	const { currentChain } = useContext(Web3Context);
	const { erc20List, requestERC20Token } = useContext(ERC20Context);

	const getCollateralItem = (item: CollateralItem) => {
		if (!currentChain) {
			return null;
		}

		// native token
		if (item.assetType === _AssetType.native && item.amount) {
			return (
				<span className='i-coin' key={'native'}>
					<img src={currentChain.tokenIcon || default_icon} alt='' />
				</span>
			);
		}

		if (item.assetType === _AssetType.FA && item.amount) {
			// Common ERC20
			const foundERC20 = erc20List.find((iitem: ERC20Type) => {
				return (
					item.contractAddress.toLowerCase() ===
					iitem.contractAddress.toLowerCase()
				);
			});
			if (foundERC20) {
				// known ERC20
				return (
					<span className='i-coin' key={foundERC20.contractAddress}>
						<img src={foundERC20.icon || default_icon} alt='' />
					</span>
				);
			} else {
				// unknown ERC20
				requestERC20Token(item.contractAddress);
				return (
					<span className='i-coin' key={item.contractAddress}>
						<img src={default_icon} alt='' />
					</span>
				);
			}
		}

		if (item.assetType === _AssetType.DA) {
			return (
				<span className='i-coin' key={`${item.contractAddress}${item.tokenId}`}>
					<img src={item.tokenImg || default_nft} alt='' />
				</span>
			);
		}
/*
		if (item.assetType === _AssetType.ERC1155) {
			return (
				<span className='i-coin' key={`${item.contractAddress}${item.tokenId}`}>
					<img src={item.tokenImg || default_nft} alt='' />
				</span>
			);
		}*/

		return null;
	};

	return (
		<div className='coins'>
			{tokens
				.sort((item, prev) => {
					if (item.assetType < prev.assetType) {
						return -1;
					}
					if (item.assetType > prev.assetType) {
						return 1;
					}

					if (
						item.contractAddress.toLowerCase() <
						prev.contractAddress.toLowerCase()
					) {
						return -1;
					}
					if (
						item.contractAddress.toLowerCase() >
						prev.contractAddress.toLowerCase()
					) {
						return 1;
					}

					if (item.tokenId && prev.tokenId) {
						try {
							if (
								new BigNumber(item.tokenId).isNaN() ||
								new BigNumber(prev.tokenId).isNaN()
							) {
								if (parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`)) {
									return -1;
								}
								if (parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`)) {
									return 1;
								}
							}
							const itemTokenIdNumber = new BigNumber(item.tokenId);
							const prevTokenIdNumber = new BigNumber(prev.tokenId);

							if (itemTokenIdNumber.lt(prevTokenIdNumber)) {
								return -1;
							}
							if (itemTokenIdNumber.gt(prevTokenIdNumber)) {
								return 1;
							}
						} catch (ignored) {
							if (
								`${item.tokenId}`.toLowerCase() <
								`${prev.tokenId}`.toLowerCase()
							) {
								return -1;
							}
							if (
								`${item.tokenId}`.toLowerCase() >
								`${prev.tokenId}`.toLowerCase()
							) {
								return 1;
							}
						}
					}

					return 0;
				})
				.slice(0, 10)
				.map((item) => {
					return getCollateralItem(item);
				})}
		</div>
	);
}
