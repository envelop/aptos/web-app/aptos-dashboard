import { useContext, useEffect, useRef, useState } from 'react';
import { matchRoutes, useLocation } from 'react-router-dom';
import {
	NFT,
	NFTorWNFT,
	WNFT,
	WNFTsStat,
	_AssetType,
	appendWNFTToStat,
	castToWNFT,
} from '../../aptos';

import {
	fetchTokenJSON,
	getUserNFTsFromAPI,
	getUserWNFTsFromAPI,
	getWNFTsOfContractFromChain721,
	getWNFTByIdFromChain,
	getWNFTsOfContractFromChain,
	getNFTsOfContractFromChain,
} from '../../aptos';

import { Web3Context } from '../../dispatchers';

import { TokenRenderType } from '../NFTCard';

import TokenList from '../TokenList';
import CollateralStat from '../CollateralStat';

import icon_loading from '../../static/pics/loading.svg';

import config from '../../app.config.json';

export default function Dashboard() {
	const loadTokens = useRef(true);

	const [currentPage, setCurrentPage] = useState<
		'wrapped' | 'discovered' | 'collateral'
	>('discovered');

	const [tokens, setTokens] = useState<{
		wrapped: Array<WNFT>;
		discovered: Array<NFT>;
	}>({ wrapped: [], discovered: [] });
	const [wrappedTokensFilteredCount, setWrappedTokensFilteredCount] = useState<
		number | undefined
	>(undefined);
	const [discoveredTokensFilteredCount, setDiscoveredTokensFilteredCount] =
		useState<number | undefined>(undefined);

	const [collateralsStat, setCollateralsStat] = useState<WNFTsStat | undefined>(
		undefined,
	);

	const [wrappedTokensLoading, setWrappedTokensLoading] = useState(true);
	const waitForWalletDelay = 5; // s
	setTimeout(() => {
		if (!userAddress) {
			setWrappedTokensLoading(false);
		}
	}, waitForWalletDelay * 1000);

	const { userAddress, currentChainId, getWeb3Force } = useContext(Web3Context);

	const location = useLocation();

	const pendingTokens = useRef<Array<string>>([]);
	const updateTokenJSON = (token: NFTorWNFT) => {
		if (!token.tokenUrl) {
			return;
		}
		if (token.image !== undefined) {
			return;
		}
		if (
			pendingTokens.current.includes(
				`${token.contractAddress.toLowerCase()}${token.contractAddress.toLowerCase()}`,
			)
		) {
			return;
		}

		pendingTokens.current = [
			...pendingTokens.current,
			`${token.contractAddress.toLowerCase()}${token.contractAddress.toLowerCase()}`,
		];
		fetchTokenJSON(token.tokenUrl).then((data) => {
			if (!data) {
				return;
			}

			setTokens((prevState) => {
				const foundInWrapped = prevState.wrapped.find((item) => {
					return (
						item.collectionAddress.toLowerCase() ===
							token.contractAddress.toLowerCase() &&
						item.tokenId.toLowerCase() === token.tokenId.toLowerCase()
					);
				});
				if (foundInWrapped) {
					return {
						wrapped: [
							...prevState.wrapped.filter((item) => {
								return (
									item.collectionAddress.toLowerCase() !==
										token.contractAddress.toLowerCase() ||
									item.tokenId.toLowerCase() !== token.tokenId.toLowerCase()
								);
							}),
							castToWNFT({
								...token,
								...data,
							}),
						],
						discovered: prevState.discovered,
					};
				}

				const foundInDiscovered = prevState.discovered.find((item) => {
					return (
						item.collectionAddress.toLowerCase() ===
							token.contractAddress.toLowerCase() &&
						item.tokenId.toLowerCase() === token.tokenId.toLowerCase()
					);
				});
				if (foundInDiscovered) {
					return {
						discovered: [
							...prevState.discovered.filter((item) => {
								return (
									item.collectionAddress.toLowerCase() !==
										token.contractAddress.toLowerCase() ||
									item.tokenId.toLowerCase() !== token.tokenId.toLowerCase()
								);
							}),
							{
								...token,
								...data,
							},
						],
						wrapped: prevState.wrapped,
					};
				}

				return prevState;
			});

			pendingTokens.current = pendingTokens.current.filter((item) => {
				return (
					item !==
					`${token.contractAddress.toLowerCase()}${token.contractAddress.toLowerCase()}`
				);
			});
		});
	};

	// fetch tokens
	useEffect(() => {
		const fetchRecentlyWrapedWNFT = async () => {
			if (!userAddress) {
				return;
			}
			console.log('fetchWNFT start');
			let tokenWnftId;

			const sourceParams = [
				//			{ path: '/:chainId/:contractAddress/:tokenId' },
				{ path: '/:chainId/:tokenId' },
				{ path: '/:chainId' },
			];
			const matches = matchRoutes(sourceParams, location);

			if (
				matches &&
				matches[0] &&
				matches[0].params &&
				matches[0].params.chainId &&
				parseInt(matches[0].params.chainId)
			) {
				if (matches[0].params.tokenId) {
					tokenWnftId = matches[0].params.tokenId;
				}
			}
			if (!tokenWnftId) {
				return;
			}
			let wnfts: Array<WNFT> = [];
			let wnft = await getWNFTByIdFromChain(
				currentChainId,
				tokenWnftId,
				userAddress,
			);
			//			console.log('fetchWNFT wnft', wnft);
			//			console.log('fetchWNFT userAddress', userAddress);
			//			console.log('fetchWNFT wnft.owner!=userAddress', wnft.owner!=userAddress);
			if (wnft.owner != userAddress) {
				return;
			}

			wnfts.push(wnft);
			console.log('fetchWNFT wnfts', wnfts);

			setTokens((prevState) => {
				console.log('fetchWNFT prevState', prevState);

				if (!loadTokens.current) {
					return prevState;
				}
				return {
					wrapped: [
						...prevState.wrapped.filter((item) => {
							return !wnfts.find((iitem) => {
								return (
									item.collectionAddress.toLowerCase() ===
										iitem.collectionAddress.toLowerCase() &&
									item.tokenId === iitem.tokenId
								);
							});
						}),
						...wnfts,
					],
					discovered: prevState.discovered.filter((item) => {
						return !wnfts.find((iitem) => {
							return (
								item.collectionAddress.toLowerCase() ===
									iitem.collectionAddress.toLowerCase() &&
								item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
							);
						});
					}),
				};
			});
		};

		const fetchWNFTs721 = async (page: number) => {
			if (currentChainId === 0) {
				return;
			}
			if (!userAddress) {
				return;
			}
			if (!loadTokens.current) {
				return;
			}

			let wnfts = await getUserWNFTsFromAPI(
				currentChainId,
				_AssetType.DA,
				userAddress,
				page,
				{ tokensOnPage: 50 },
			);
			//			wnfts = await addRecentlywrapedToken(wnfts);
			setWrappedTokensFilteredCount(undefined);
			setTokens((prevState) => {
				console.log('fetchWNFTs721 prevState', prevState);

				if (!loadTokens.current) {
					return prevState;
				}
				return {
					wrapped: [
						...prevState.wrapped.filter((item) => {
							return !wnfts.find((iitem) => {
								return (
									item.collectionAddress.toLowerCase() ===
										iitem.collectionAddress.toLowerCase() &&
									item.tokenId === iitem.tokenId
								);
							});
						}),
						...wnfts,
					],
					discovered: prevState.discovered.filter((item) => {
						return !wnfts.find((iitem) => {
							return (
								item.collectionAddress.toLowerCase() ===
									iitem.collectionAddress.toLowerCase() &&
								item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
							);
						});
					}),
				};
			});
			setWrappedTokensLoading(false);

			console.log('page', page);
			console.log('wnfts.length', wnfts.length);

			if (page === 1 && wnfts.length === 0) {
				// chain fallback

				const foundChainData = config.CHAIN_SPECIFIC_DATA.find((item) => {
					return item.chainId == currentChainId;
				});
				console.log('foundChainData', foundChainData);

				if (!foundChainData) {
					return;
				}
				const storages = foundChainData.WNFTStorageContracts.filter((item) => {
					return item.assetType === '-1';
				}).flatMap((item) => {
					//					return item.contractAddress;
					return item.collectionAddress;
				});
				console.log('storages', storages);

				// requests per contract
				storages.forEach((item) => {
					//getWNFTsOfContractFromChain721(
					getWNFTsOfContractFromChain(currentChainId, item, userAddress).then(
						(data) => {
							setWrappedTokensFilteredCount(undefined);
							setWrappedTokensLoading(false);

							console.log('data', data);
							console.log('oldTokens', tokens);

							let newTokens = {
								wrapped: [
									...tokens.wrapped.filter((item) => {
										return !data.find((iitem) => {
											return (
												item.collectionAddress.toLowerCase() ===
													iitem.collectionAddress.toLowerCase() &&
												item.tokenId === iitem.tokenId
											);
										});
									}),
									...data,
								],
								discovered: tokens.discovered.filter((item) => {
									return !data.find((iitem) => {
										return (
											item.collectionAddress.toLowerCase() ===
												iitem.collectionAddress.toLowerCase() &&
											item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
										);
									});
								}),
							};
							console.log('newTokens', newTokens);

							setTokens((prevState) => {
								return {
									wrapped: [
										...prevState.wrapped.filter((item) => {
											return !data.find((iitem) => {
												return (
													item.collectionAddress.toLowerCase() ===
														iitem.collectionAddress.toLowerCase() &&
													item.tokenId === iitem.tokenId
												);
											});
										}),
										...data,
									],
									discovered: prevState.discovered.filter((item) => {
										return !data.find((iitem) => {
											return (
												item.collectionAddress.toLowerCase() ===
													iitem.collectionAddress.toLowerCase() &&
												item.tokenId.toLowerCase() ===
													iitem.tokenId.toLowerCase()
											);
										});
									}),
								};
							});
						},
					);
				});

				// // batch request, less responsive
				// getWNFTsOfContractFromChain721Batch(currentChainId, storages, userAddress)
				// 	.then((data) => {
				// 		setWrappedTokensFilteredCount(undefined);
				// 		setTokens((prevState) => {
				// 			return {
				// 				wrapped: [
				// 					...prevState.wrapped,
				// 					...data,
				// 				],
				// 				discovered: prevState.discovered.filter((item) => {
				// 					return !wnfts.find((iitem) => {
				// 						return item.collectionAddress.toLowerCase() === iitem.collectionAddress.toLowerCase() &&
				// 						item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
				// 					})
				// 				}),
				// 			}
				// 		})
				// 	});
				// setWrappedTokensLoading(false);
			}
		};
		/*
		const fetchWNFTs1155 = async (page: number) => {
			if (currentChainId === 0) {
				return;
			}
			if (!userAddress) {
				return;
			}
			if (!loadTokens.current) {
				return;
			}

			const wnfts = await getUserWNFTsFromAPI(
				currentChainId,
				_AssetType.ERC1155,
				userAddress,
				page,
				{ tokensOnPage: 50 },
			);
			setWrappedTokensFilteredCount(undefined);
			setTokens((prevState) => {
				if (!loadTokens.current) {
					return prevState;
				}
				return {
					wrapped: [
						...prevState.wrapped.filter((item) => {
							return !wnfts.find((iitem) => {
								return (
									item.collectionAddress.toLowerCase() ===
										iitem.collectionAddress.toLowerCase() &&
									item.tokenId === iitem.tokenId
								);
							});
						}),
						...wnfts,
					],
					discovered: prevState.discovered.filter((item) => {
						return !wnfts.find((iitem) => {
							return (
								item.collectionAddress.toLowerCase() ===
									iitem.collectionAddress.toLowerCase() &&
								item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
							);
						});
					}),
				};
			});
			setWrappedTokensLoading(false);
		};
		*/
		const fetchNFTs721FromChain = async (page: number) => {
			if (currentChainId === 0) {
				return;
			}
			if (!userAddress) {
				return;
			}
			if (!loadTokens.current) {
				return;
			}

			const nfts = await getNFTsOfContractFromChain(
				currentChainId,
				userAddress,
				page,
				100,
			);
			setDiscoveredTokensFilteredCount(undefined);

			let onchaintoken721 = {
				discovered: [
					...tokens.discovered.filter((item) => {
						return !nfts.find((iitem) => {
							return (
								item.collectionAddress.toLowerCase() ===
									iitem.collectionAddress.toLowerCase() &&
								item.tokenId === iitem.tokenId
							);
						});
					}),
					...nfts,
				],
				wrapped: tokens.wrapped,
			};
			console.log('onchaintoken721', onchaintoken721);

			setTokens((prevState) => {
				if (!loadTokens.current) {
					return prevState;
				}
				return {
					discovered: [
						...prevState.discovered.filter((item) => {
							return !nfts.find((iitem) => {
								return (
									item.collectionAddress.toLowerCase() ===
										iitem.collectionAddress.toLowerCase() &&
									item.tokenId === iitem.tokenId
								);
							});
						}),
						...nfts,
					],
					wrapped: prevState.wrapped,
				};
			});
			setWrappedTokensLoading(false);

			if (nfts.length) {
				await fetchNFTs721FromChain(page + 1);
			}
		};

		const fetchNFTs721 = async (page: number) => {
			if (currentChainId === 0) {
				return;
			}
			if (!userAddress) {
				return;
			}
			if (!loadTokens.current) {
				return;
			}

			const nfts = await getUserNFTsFromAPI(
				currentChainId,
				_AssetType.DA,
				userAddress,
				page,
				{ filterWNFTS: true, tokensOnPage: 100 },
			);
			setDiscoveredTokensFilteredCount(undefined);
			setTokens((prevState) => {
				if (!loadTokens.current) {
					return prevState;
				}
				return {
					discovered: [
						...prevState.discovered.filter((item) => {
							return !nfts.find((iitem) => {
								return (
									item.collectionAddress.toLowerCase() ===
										iitem.collectionAddress.toLowerCase() &&
									item.tokenId === iitem.tokenId
								);
							});
						}),
						...nfts,
					],
					wrapped: prevState.wrapped,
				};
			});
			setWrappedTokensLoading(false);

			if (nfts.length) {
				await fetchNFTs721(page + 1);
			} else {
				await fetchNFTs721FromChain(1);
			}
		};
		/*
		const fetchNFTs1155 = async (page: number) => {
			if (currentChainId === 0) {
				return;
			}
			if (!userAddress) {
				return;
			}
			if (!loadTokens.current) {
				return;
			}

			const nfts = await getUserNFTsFromAPI(
				currentChainId,
				_AssetType.ERC1155,
				userAddress,
				page,
				{ filterWNFTS: true, tokensOnPage: 100 },
			);
			setDiscoveredTokensFilteredCount(undefined);
			setTokens((prevState) => {
				if (!loadTokens.current) {
					return prevState;
				}
				return {
					discovered: [
						...prevState.discovered.filter((item) => {
							return !nfts.find((iitem) => {
								return (
									item.collectionAddress.toLowerCase() ===
										iitem.collectionAddress.toLowerCase() &&
									item.tokenId === iitem.tokenId
								);
							});
						}),
						...nfts,
					],
					wrapped: prevState.wrapped,
				};
			});
			setWrappedTokensLoading(false);
			if (nfts.length) {
				fetchNFTs1155(page + 1);
			}
		};
*/
		loadTokens.current = !!userAddress;

		setTokens({ wrapped: [], discovered: [] });
		setCollateralsStat(undefined);
		fetchRecentlyWrapedWNFT();
		fetchNFTs721(1);
		//		fetchNFTs1155(1);
		fetchWNFTs721(1);
		//		fetchWNFTs1155(1);
		console.log('prevState', tokens);
	}, [userAddress, currentChainId]);

	// calc stats
	useEffect(() => {
		let outStat: WNFTsStat | undefined;

		tokens.wrapped.forEach((item) => {
			console.log('setCollateralsStat token', item);
			let castWNFT = castToWNFT(item);
			console.log('setCollateralsStat castWNFT', castWNFT);
			outStat = appendWNFTToStat(castToWNFT(item), outStat);
			console.log('setCollateralsStat outStat', outStat);
		});
		if (outStat) {
			console.log('setCollateralsStat', outStat);

			setCollateralsStat(outStat);
		}
	}, [tokens]);

	//post filtered nft -> wnft
	useEffect(() => {
		let newTokens = {
			wrapped: [...tokens.wrapped],
			discovered: tokens.discovered.filter((item) => {
				return !tokens.wrapped.find((iitem) => {
					return (
						item.collectionAddress.toLowerCase() ===
							iitem.collectionAddress.toLowerCase() &&
						item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
					);
				});
			}),
		};
		if (
			newTokens.wrapped.length != tokens.wrapped.length ||
			newTokens.discovered.length != tokens.discovered.length
		) {
			setTokens(newTokens);
		}
		/*
		setTokens((prevState) => {
			return {
				wrapped: [...prevState.wrapped],
				discovered: prevState.discovered.filter((item) => {
					return !prevState.wrapped.find((iitem) => {
						return (
							item.collectionAddress.toLowerCase() ===
								iitem.collectionAddress.toLowerCase() &&
							item.tokenId.toLowerCase() === iitem.tokenId.toLowerCase()
						);
					});
				}),
			};
		});
		*/
	}, [tokens]);

	// filter discovered tokens by wrapped
	useEffect(() => {
		if (currentPage === 'wrapped' && !tokens.wrapped.length) {
			setCurrentPage('discovered');
		}
		if (currentPage === 'collateral' && !collateralsStat) {
			setCurrentPage('discovered');
		}
		if (currentPage === 'discovered' && !tokens.discovered.length) {
			if (tokens.wrapped.length) {
				setCurrentPage('wrapped');
			}
		}
	}, [tokens, collateralsStat]);

	const getWrappedTokensCount = () => {
		if (!tokens.wrapped.length) {
			return '';
		}

		if (
			wrappedTokensFilteredCount === undefined ||
			tokens.wrapped.length === wrappedTokensFilteredCount
		) {
			return <span>{tokens.wrapped.length}</span>;
		}

		return (
			<span>
				{wrappedTokensFilteredCount} / {tokens.wrapped.length}
			</span>
		);
	};
	const getDiscoveredTokensCount = () => {
		if (!tokens.discovered.length) {
			return '';
		}

		if (
			discoveredTokensFilteredCount === undefined ||
			tokens.discovered.length === discoveredTokensFilteredCount
		) {
			return <span>{tokens.discovered.length}</span>;
		}

		return (
			<span>
				{discoveredTokensFilteredCount} / {tokens.discovered.length}
			</span>
		);
	};
	const getCollateralCount = () => {
		if (!collateralsStat) {
			return null;
		}
		if (collateralsStat.collaterals.length === 0) {
			return '';
		}

		return <span>{collateralsStat.collaterals.length}</span>;
	};

	const getCurrentPage = () => {
		if (wrappedTokensLoading) {
			return (
				<div className='db-section'>
					<div className='container'>
						<div className='lp-list__footer'>
							<img className='loading' src={icon_loading} alt='' />
						</div>
					</div>
				</div>
			);
		}

		if (!userAddress) {
			return (
				<div className='db-section'>
					<div className='container'>
						<div className='row justify-content-center'>
							<div className='col-auto'>
								<button
									className='btn btn-lg btn-outline'
									onClick={async () => {
										try {
											await getWeb3Force();
										} catch (e: any) {
											console.log('Cannot connect', e);
										}
									}}
								>
									Connect to&nbsp;your wallet to&nbsp;load&nbsp;tokens
								</button>
							</div>
						</div>
					</div>
				</div>
			);
		}

		if (!tokens.discovered.length && !tokens.wrapped.length) {
			return (
				<div className='db-section'>
					<div className='container'>
						<div className='lp-list__footer'>
							<div className='nomore'>There's no more wDA (wNFT) yet</div>
						</div>
					</div>
				</div>
			);
		}

		if (currentPage === 'wrapped') {
			return (
				<TokenList
					key={'wrapped'}
					tokens={tokens.wrapped}
					updateFilteredCount={(count) => {
						setWrappedTokensFilteredCount(count);
					}}
					tokenRenderType={TokenRenderType.wrapped}
					onTokenRender={updateTokenJSON}
				/>
			);
		}
		if (currentPage === 'discovered') {
			return (
				<TokenList
					key={'discovered'}
					tokens={tokens.discovered}
					updateFilteredCount={(count) => {
						setDiscoveredTokensFilteredCount(count);
					}}
					tokenRenderType={TokenRenderType.discovered}
					onTokenRender={updateTokenJSON}
				/>
			);
		}
		if (currentPage === 'collateral' && collateralsStat) {
			return <CollateralStat collateralStat={collateralsStat} />;
		}
		return null;
	};
	const getPageSelector = () => {
		// if ( !userAddress ) { return null; }

		return (
			<div className='db-section__toggle'>
				{tokens.discovered.length ? (
					<button
						className={`tab ${currentPage === 'discovered' ? 'active' : ''}`}
						onClick={() => {
							setCurrentPage('discovered');
						}}
					>
						DA (NFT) {getDiscoveredTokensCount()}
					</button>
				) : null}
				{tokens.wrapped.length ? (
					<button
						className={`tab ${currentPage === 'wrapped' ? 'active' : ''}`}
						onClick={() => {
							setCurrentPage('wrapped');
						}}
					>
						wDA (wNFT) {getWrappedTokensCount()}
					</button>
				) : null}
				{collateralsStat && collateralsStat.collaterals.length ? (
					<button
						className={`tab ${currentPage === 'collateral' ? 'active' : ''}`}
						onClick={() => {
							setCurrentPage('collateral');
						}}
					>
						Collateral {getCollateralCount()}
					</button>
				) : null}
			</div>
		);
	};

	const getInfoText = () => {
		return (
			<div className='db-info'>
				<div className='divider right'></div>
				<div className='container mt-6'>
					<div className='row'>
						<div className='col-12 col-md-8'>
							<div className='c-wrap'>
								<p>
									<b>NFT 2.0&nbsp;is known under many names</b>: Smart, Wrapped,
									Programmable, etc.{' '}
								</p>
								<p>
									<b>
										DAO Envelop (2020) is&nbsp;the pioneer of&nbsp;this trend.
									</b>
									<br /> You can do&nbsp;a&nbsp;lot with Envelop applications:
									create wrapped NFT, collateralize ERC-20 tokens and native
									coins (and NFTs in&nbsp;some special cases), and use complex
									mechanics like Cross NFT, which are extending to&nbsp;ZKP very
									soon (2023).
								</p>
							</div>
						</div>
						<div className='col-12 col-md-4'>
							<div className='text-small'>
								<p>
									If&nbsp;you have constructive suggestions on&nbsp;how
									to&nbsp;improve Envelop applications, you are welcome
									to&nbsp;participate in&nbsp;the bounty campaign. Envelop
									administrator is&nbsp;always in&nbsp;touch. Write and develop
									a&nbsp;new market with&nbsp;us!
								</p>
								<a
									className='btn btn-sm btn-border'
									href='https://t.me/TW4Ys'
									target='_blank'
									rel='noopener noreferrer'
								>
									Write to Envelop
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};

	return (
		<main className='s-main'>
			<div className='container mt-4'>
				<div className='row mb-6'>
					<div className='col-12 col-sm-4 col-md-3 order-sm-2 mb-4 mb-sm-0'>
						<a className='btn btn-grad w-100' href='/wrap'>
							Free Wrap
						</a>
					</div>

					<div className='col-12 col-md order-md-1'>{getPageSelector()}</div>
				</div>
			</div>

			{getCurrentPage()}

			{getInfoText()}
		</main>
	);
}
