import React, { useContext, useEffect, useMemo, useRef, useState } from 'react';

import {
	BigNumber,
	LockType,
	NFT,
	NFTorWNFT,
	_AssetType,
	assetTypeToString,
	chainTypeToERC20,
	compactString,
	fetchTokenJSON,
} from '../../aptos';

import { ERC20Context, Web3Context } from '../../dispatchers';

import NFTCard, { TokenRenderType } from '../NFTCard';

import config from '../../app.config.json';

import default_icon from '../../static/pics/coins/_default.svg';

type TokenListProps = {
	tokens: Array<NFTorWNFT>;
	updateFilteredCount: (count: number) => void;
	tokenRenderType: TokenRenderType;
	onTokenRender: (token: NFT) => void;
};

export default function TokenList(props: TokenListProps) {
	const { tokens, updateFilteredCount, tokenRenderType, onTokenRender } = props;

	const tokensOnPage = 12;

	const [filterByString, setFilterByString] = useState<string>('');
	const [filterByToken, setFilterByToken] = useState<Array<string>>([]);
	const [filterTimeLock, setFilterTimeLock] = useState(false);
	const [filterFee, setFilterFee] = useState(false);
	const [filterUnwrapReady, setFilterUnwrapReady] = useState(false);

	const [filterCoinsOpened, setFilterCoinsOpened] = useState(false);
	const [filterSortOpened, setFilterSortOpened] = useState(false);
	const [sortBy, setSortBy] = useState<'' | 'Address' | 'With image'>('');

	const filterCoinsRef = React.useRef<HTMLInputElement>(null);
	const filterSortRef = React.useRef<HTMLInputElement>(null);

	const [storageContracts, setStorageContracts] = useState<Array<string>>([]);
	const [currentPage, setCurrentPage] = useState<number>(0);

	const { erc20List, requestERC20Token } = useContext(ERC20Context);
	const { currentChainId, currentChain } = useContext(Web3Context);

	useEffect(() => {
		const foundChainData = config.CHAIN_SPECIFIC_DATA.find((item) => {
			return item.chainId === currentChainId;
		});
		if (foundChainData) {
			setStorageContracts(
				foundChainData.WNFTStorageContracts.flatMap((item) => {
					return item.contractAddress;
				}),
			);
		}
	}, [currentChainId]);

	const tokensFiltered = useMemo(() => {
		const filtered = tokens
			.filter((item) => {
				if (filterByString !== '') {
					if (
						item.contractAddress
							.toLowerCase()
							.includes(filterByString.toLowerCase())
					) {
						return true;
					}
					if (
						`${item.tokenId}`
							.toLowerCase()
							.includes(filterByString.toLowerCase())
					) {
						return true;
					}

					if (
						assetTypeToString(item.assetType, '')
							.toLowerCase()
							.includes(filterByString.toLowerCase())
					) {
						return true;
					}

					return false;
				}

				if (filterByToken.length) {
					if (item.fees && item.fees.length) {
						const feeFound = filterByToken.find((iitem) => {
							if (!item.fees || !item.fees.length) {
								return false;
							}
							return iitem.toLowerCase() === item.fees[0].token.toLowerCase();
						});
						if (feeFound) {
							return true;
						}
					}

					if (item.collateral && item.collateral.length) {
						const collateralFound = filterByToken.find((iitem) => {
							if (!item.collateral || !item.collateral.length) {
								return false;
							}
							return !!item.collateral.find((iiitem) => {
								return (
									iitem.toLowerCase() === iiitem.contractAddress.toLowerCase()
								);
							});
						});
						if (collateralFound) {
							return true;
						}
					}

					return false;
				}

				return true;
			})
			.filter((item) => {
				if (filterFee) {
					if (item.fees && item.fees.length) {
						return true;
					}
					return false;
				}
				if (filterTimeLock) {
					if (item.locks && item.locks.length) {
						const foundTimeLock = item.locks.find((iitem) => {
							return iitem.lockType === LockType.time;
						});
						if (foundTimeLock) {
							const now = new BigNumber(
								new Date().getTime(),
							).dividedToIntegerBy(1000);
							if (now.lt(foundTimeLock.param)) {
								return true;
							}
						}
					}

					return false;
				}
				if (filterUnwrapReady) {
					/*					if (item.assetType === _AssetType.ERC1155) {
						if (item.amount && item.totalSupply) {
							if (!item.amount.eq(item.totalSupply)) {
								return false;
							}
						}
					}
*/
					if (item.locks && item.locks.length) {
						const foundTimeLock = item.locks.find((iitem) => {
							return iitem.lockType === LockType.time;
						});
						if (foundTimeLock) {
							const now = new BigNumber(
								new Date().getTime(),
							).dividedToIntegerBy(1000);
							if (foundTimeLock.param.gt(now)) {
								return false;
							}
						}
					}

					if (item.locks && item.locks.length) {
						const foundValueLock = item.locks.find((iitem) => {
							return iitem.lockType === LockType.value;
						});
						if (foundValueLock) {
							const foundToken = erc20List.find((iitem) => {
								if (!item.fees) {
									return false;
								}
								return (
									iitem.contractAddress.toLowerCase() ===
									item.fees[0].token.toLowerCase()
								);
							});
							if (!foundToken) {
								return false;
							}

							if (
								item.collectedFees &&
								foundValueLock.param.gt(item.collectedFees)
							) {
								return false;
							}
						}
					}

					if (item.rules && item.rules.noUnwrap) {
						return false;
					}

					return true;
				}

				return true;
			});

		let sorted;
		if (sortBy === 'With image') {
			sorted = filtered.sort((item, prev) => {
				if (item.image && !prev.image) {
					return -1;
				}
				if (prev.image && !item.image) {
					return 1;
				}

				if (
					item.contractAddress.toLowerCase() <
					prev.contractAddress.toLowerCase()
				) {
					return -1;
				}
				if (
					item.contractAddress.toLowerCase() >
					prev.contractAddress.toLowerCase()
				) {
					return 1;
				}

				if (
					prev.contractAddress.toLowerCase() ===
					item.contractAddress.toLowerCase()
				) {
					try {
						if (
							new BigNumber(item.tokenId).isNaN() ||
							new BigNumber(prev.tokenId).isNaN()
						) {
							if (parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`)) {
								return 1;
							}
							if (parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`)) {
								return -1;
							}
						}
						const itemTokenIdNumber = new BigNumber(item.tokenId);
						const prevTokenIdNumber = new BigNumber(prev.tokenId);

						if (itemTokenIdNumber.lt(prevTokenIdNumber)) {
							return 1;
						}
						if (itemTokenIdNumber.gt(prevTokenIdNumber)) {
							return -1;
						}
					} catch (ignored) {
						if (
							`${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase()
						) {
							return 1;
						}
						if (
							`${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase()
						) {
							return -1;
						}
					}
				}

				return 0;
			});
		} else {
			sorted = filtered.sort((item, prev) => {
				const itemIsStorage = !!storageContracts.find((iitem) => {
					return iitem.toLowerCase() === item.contractAddress.toLowerCase();
				});
				const prevIsStorage = !!storageContracts.find((iitem) => {
					return iitem.toLowerCase() === prev.contractAddress.toLowerCase();
				});
				if (itemIsStorage && !prevIsStorage) {
					return -1;
				}
				if (prevIsStorage && !itemIsStorage) {
					return 1;
				}

				if (
					prev.contractAddress.toLowerCase() ===
					item.contractAddress.toLowerCase()
				) {
					try {
						if (
							new BigNumber(item.tokenId).isNaN() ||
							new BigNumber(prev.tokenId).isNaN()
						) {
							if (parseInt(`${item.tokenId}`) < parseInt(`${prev.tokenId}`)) {
								return 1;
							}
							if (parseInt(`${item.tokenId}`) > parseInt(`${prev.tokenId}`)) {
								return -1;
							}
						}
						const itemTokenIdNumber = new BigNumber(item.tokenId);
						const prevTokenIdNumber = new BigNumber(prev.tokenId);

						if (itemTokenIdNumber.lt(prevTokenIdNumber)) {
							return 1;
						}
						if (itemTokenIdNumber.gt(prevTokenIdNumber)) {
							return -1;
						}
					} catch (ignored) {
						if (
							`${item.tokenId}`.toLowerCase() < `${prev.tokenId}`.toLowerCase()
						) {
							return 1;
						}
						if (
							`${item.tokenId}`.toLowerCase() > `${prev.tokenId}`.toLowerCase()
						) {
							return -1;
						}
					}
				}

				return item.contractAddress
					.toLowerCase()
					.localeCompare(prev.contractAddress.toLowerCase());
			});
		}

		return sorted;
	}, [
		tokens,
		filterByString,
		filterByToken,
		filterTimeLock,
		filterFee,
		filterUnwrapReady,
		updateFilteredCount,
		erc20List,
		sortBy,
	]);

	useEffect(() => {
		const currentPageTokens = tokensFiltered.slice(
			currentPage * tokensOnPage,
			currentPage * tokensOnPage + tokensOnPage,
		);

		Promise.allSettled(
			currentPageTokens.map((item) => {
				if (onTokenRender) {
					onTokenRender(item);
				}
				return item;
			}),
		);

		if (tokensFiltered.length && !currentPageTokens.length) {
			setCurrentPage(0);
		}
		updateFilteredCount(tokensFiltered.length);
	}, [tokensFiltered, currentPage]);

	const getFilterCoinsList = () => {
		if (!filterCoinsOpened) {
			return null;
		}
		if (!currentChain) {
			return null;
		}

		return (
			<ul className='options-list list-gray'>
				{
					// TODO add 0x00
					[chainTypeToERC20(currentChain), ...erc20List]
						.map((item) => {
							return item.contractAddress;
						})
						.map((item) => {
							const added = !!filterByToken.find((iitem) => {
								return item.toLowerCase() === iitem.toLowerCase();
							});
							const foundToken =
								item === '0x0000000000000000000000000000000000000000'
									? chainTypeToERC20(currentChain)
									: erc20List.find((iitem) => {
											return (
												iitem.contractAddress.toLowerCase() ===
												item.toLowerCase()
											);
									  });

							if (added) {
								return (
									<li
										key={item}
										className='option selected'
										onClick={() => {
											setFilterByToken(
												filterByToken.filter((iitem) => {
													return iitem.toLowerCase() !== item.toLowerCase();
												}),
											);
										}}
									>
										<div className='option-coin'>
											<span className='i-coin'>
												<img src={foundToken?.icon || default_icon} alt='' />
											</span>
											<span className='name'>
												{foundToken?.symbol || compactString(item)}
											</span>
										</div>
									</li>
								);
							} else {
								return (
									<li
										key={item}
										className='option'
										onClick={() => {
											setFilterByToken([...filterByToken, item]);
										}}
									>
										<div className='option-coin'>
											<span className='i-coin'>
												<img src={foundToken?.icon || default_icon} alt='' />
											</span>
											<span className='name'>
												{foundToken?.symbol || compactString(item)}
											</span>
										</div>
									</li>
								);
							}
						})
				}
			</ul>
		);
	};
	const openFilterCoinsList = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if (!body) {
				return;
			}
			body.onclick = (e: any) => {
				if (!filterCoinsRef.current) {
					return;
				}
				const _path = e.composedPath() || e.path;
				if (_path && _path.includes(filterCoinsRef.current)) {
					return;
				}
				closeFilterCoinsList();
			};
		}, 100);
		setFilterCoinsOpened(true);
	};
	const closeFilterCoinsList = () => {
		const body = document.querySelector('body');
		if (!body) {
			return;
		}
		body.onclick = null;
		setFilterCoinsOpened(false);
	};
	const getFilterSortList = () => {
		if (!filterSortOpened) {
			return null;
		}

		return (
			<ul className='options-list list-gray' style={{ display: 'block' }}>
				<li
					className='option'
					onClick={() => {
						setSortBy('Address');
						setCurrentPage(0);
						closeFilterSortList();
					}}
				>
					Address
				</li>
				<li
					className='option'
					onClick={() => {
						setSortBy('With image');
						setCurrentPage(0);
						closeFilterSortList();
					}}
				>
					With image
				</li>
			</ul>
		);
	};
	const openFilterSortList = () => {
		setTimeout(() => {
			const body = document.querySelector('body');
			if (!body) {
				return;
			}
			body.onclick = (e: any) => {
				if (!filterSortRef.current) {
					return;
				}
				const _path = e.composedPath() || e.path;
				if (_path && _path.includes(filterSortRef.current)) {
					return;
				}
				closeFilterSortList();
			};
		}, 100);
		setFilterSortOpened(true);
	};
	const closeFilterSortList = () => {
		const body = document.querySelector('body');
		if (!body) {
			return;
		}
		body.onclick = null;
		setFilterSortOpened(false);
	};
	const getFilterBlock = () => {
		if (!currentChain) {
			return null;
		}

		let searchFieldClazz = 'col-12 col-md-5 col-lg-6 mb-4';
		let selectTokensClazz = 'col-12 col-sm-8 col-md-4 col-lg-4 mb-4';
		let sortFieldClazz = 'col-12 col-sm-4 col-md-3 col-lg-2 mb-4';

		if (tokenRenderType === TokenRenderType.discovered) {
			searchFieldClazz = 'col-12 col-sm-8 col-md-8 col-lg-8 mb-4';
			sortFieldClazz = 'col-12 col-sm-4 col-md-4 col-lg-4 mb-4';
		}

		return (
			<div className='db-filter'>
				<div className='row'>
					<div className={searchFieldClazz}>
						<input
							className='input-control control-gray control-search'
							type='text'
							placeholder='DA (NFT) Address, ID...'
							value={filterByString}
							onChange={(e) => {
								const strUpdated = e.target.value;
								setFilterByString(strUpdated);
							}}
						/>
					</div>
					{tokenRenderType === TokenRenderType.wrapped ? (
						<div className={selectTokensClazz}>
							<div
								className='select-custom select-collateral'
								ref={filterCoinsRef}
							>
								<div
									onClick={() => {
										if (filterCoinsOpened) {
											closeFilterCoinsList();
										} else {
											openFilterCoinsList();
										}
									}}
									className={`input-control control-gray ${
										filterCoinsOpened ? 'active' : ''
									}`}
								>
									{filterByToken.length ? (
										<span className='coins'>
											{filterByToken.map((item) => {
												const foundToken =
													item === '0x0000000000000000000000000000000000000000'
														? chainTypeToERC20(currentChain)
														: erc20List.find((iitem) => {
																return (
																	iitem.contractAddress.toLowerCase() ===
																	item.toLowerCase()
																);
														  });
												let icon = default_icon;
												if (foundToken) {
													icon = foundToken.icon;
												} else {
													requestERC20Token(item);
												}
												return (
													<span className='i-coin'>
														<img src={icon} alt='' />
													</span>
												);
											})}
										</span>
									) : (
										<span className='empty'>Select tokens</span>
									)}
								</div>

								{getFilterCoinsList()}
							</div>
						</div>
					) : null}
					<div className={sortFieldClazz}>
						<div className='select-custom' ref={filterSortRef}>
							<div
								className={`input-control control-gray ${
									filterSortOpened ? 'active' : ''
								}`}
								onClick={() => {
									if (filterSortOpened) {
										closeFilterSortList();
									} else {
										openFilterSortList();
									}
								}}
							>
								<span className='empty'>
									{sortBy === '' ? 'Sort by' : sortBy}
								</span>
							</div>
							{getFilterSortList()}
						</div>
					</div>
					<div className='col-12'>
						<div className='row'>
							<div className='col-auto mb-2'>
								<label className='checkbox'>
									<input
										type='checkbox'
										onChange={() => {
											setFilterTimeLock(!filterTimeLock);
										}}
									/>
									<span className='check'></span>
									<span className='check-text'>TimeLock</span>
								</label>
							</div>
							<div className='col-auto mb-2'>
								<label className='checkbox'>
									<input
										type='checkbox'
										onChange={(e) => {
											setFilterUnwrapReady(!filterUnwrapReady);
										}}
									/>
									<span className='check'> </span>
									<span className='check-text'>Ready to unwrap</span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};
	const getListBody = () => {
		const getYourTokenPagination = () => {
			const pagesCount = Math.ceil(tokensFiltered.length / tokensOnPage);
			if (pagesCount < 2) {
				return null;
			}
			return (
				<div className='pagination'>
					<ul>
						<li>
							<button
								className={`arrow ${currentPage === 0 ? 'disabled' : ''}`}
								onClick={(e) => {
									e.preventDefault();
									if (currentPage === 0) {
										return;
									}
									setCurrentPage(currentPage - 1);
								}}
							>
								<svg
									width='40'
									height='40'
									viewBox='0 0 40 40'
									fill='none'
									xmlns='http://www.w3.org/2000/svg'
								>
									<path
										fillRule='evenodd'
										clipRule='evenodd'
										d='M24.0684 10.3225C24.504 10.7547 24.5067 11.4582 24.0744 11.8938L15.9623 20.0679L23.9397 28.1062C24.372 28.5418 24.3693 29.2453 23.9338 29.6775C23.4982 30.1098 22.7947 30.1071 22.3624 29.6716L13.6082 20.8505C13.1783 20.4173 13.1783 19.7184 13.6082 19.2852L22.4971 10.3284C22.9294 9.89287 23.6329 9.89019 24.0684 10.3225Z'
										fill='white'
									></path>
								</svg>
							</button>
						</li>
						{Array.from({ length: pagesCount }).map((item, idx) => {
							return (
								<li key={idx}>
									<button
										className={currentPage === idx ? 'active' : ''}
										onClick={(e) => {
											e.preventDefault();
											setCurrentPage(idx);
										}}
									>
										{idx + 1}
									</button>
								</li>
							);
						})}
						<li>
							<button
								className={`arrow ${
									currentPage === pagesCount - 1 ? 'disabled' : ''
								}`}
								onClick={(e) => {
									e.preventDefault();
									if (currentPage === pagesCount - 1) {
										return;
									}
									setCurrentPage(currentPage + 1);
								}}
							>
								<svg
									width='40'
									height='40'
									viewBox='0 0 40 40'
									fill='none'
									xmlns='http://www.w3.org/2000/svg'
								>
									<path
										fillRule='evenodd'
										clipRule='evenodd'
										d='M15.9316 29.6775C15.496 29.2453 15.4933 28.5418 15.9256 28.1062L24.0377 19.9321L16.0603 11.8938C15.628 11.4582 15.6307 10.7547 16.0662 10.3225C16.5018 9.89019 17.2053 9.89287 17.6376 10.3284L26.3918 19.1495C26.8217 19.5827 26.8217 20.2816 26.3918 20.7148L17.5029 29.6716C17.0706 30.1071 16.3671 30.1098 15.9316 29.6775Z'
										fill='white'
									></path>
								</svg>
							</button>
						</li>
					</ul>
				</div>
			);
		};

		return (
			<React.Fragment>
				<div className='alert alert-warning text-center mb-4'>
					To correctly show your DAs (NFTs), <b>please disable Shield</b>{' '}
					in&nbsp;Brave browser or&nbsp;other ad&nbsp;blockers
				</div>
				<div className='c-row'>
					{tokensFiltered
						.slice(
							currentPage * tokensOnPage,
							currentPage * tokensOnPage + tokensOnPage,
						)
						.map((item: NFTorWNFT) => {
							return (
								<div
									className='c-col'
									key={`${item.contractAddress}${item.tokenId}`}
								>
									<NFTCard token={item} tokenRenderType={tokenRenderType} />
								</div>
							);
						})}
				</div>
				{getYourTokenPagination()}
			</React.Fragment>
		);
	};

	return (
		<React.Fragment>
			<div className='db-section'>
				<div className='container'>
					{getFilterBlock()}
					{getListBody()}
				</div>
			</div>
		</React.Fragment>
	);
}
