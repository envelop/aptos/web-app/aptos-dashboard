
import Header from '../Header';
import Footer from '../Footer';

import {
	ERC20Dispatcher,
	InfoModalDispatcher,
	Web3Dispatcher
} from '../../dispatchers';
import Dashboard from '../Dashboard';

import { PetraWallet } from "petra-plugin-wallet-adapter";
import { AptosWalletAdapterProvider } from "@aptos-labs/wallet-adapter-react";


export default function App() {

	const wallets = [new PetraWallet()];

	return (
		<>
			<InfoModalDispatcher>
			<AptosWalletAdapterProvider plugins={wallets} autoConnect={true}>
			<Web3Dispatcher switchChainCallback={() => { window.location.reload() }}>
			<ERC20Dispatcher>

				<Header />
				<Dashboard />
				<Footer />

			</ERC20Dispatcher>
			</Web3Dispatcher>
			</AptosWalletAdapterProvider>
			</InfoModalDispatcher>
		</>
	)

}